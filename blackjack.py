############### Our Blackjack House Rules #####################

## The deck is unlimited in size. 
## There are no jokers. 
## The Jack/Queen/King all count as 10.
## The the Ace can count as 11 or 1.
## Use the following list as the deck of cards:
## cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
## The cards in the list have equal probability of being drawn.
## Cards are not removed from the deck as they are drawn.
## The computer is the dealer.

###############################################################

import random
from art import logo
def deal_card(): 
    """Returns a random card from the deck."""
    cards = [11, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10, 10, 10]
    card = random.choice(cards)
    return card
def callculate_score(cards):
    """Calculates the result"""

    if 11 in cards and sum(cards) > 21:
        cards.remove(11)
        cards.append(1)
    if len(cards) == 2 and sum(cards) == 21:
        return 0
    else:
        return sum(cards)
def compare(user_score, computer_score):
    if user_score == computer_score:
        return "Draw"
    elif computer_score == 0:
        return "Lose, opponent has Blackjack"
    elif user_score == 0:
        return "Win with a Blackjack"
    elif computer_score > 21:
        return "Opponent went over. You win"
    elif user_score > computer_score:
        return "You win"
    else:
        return "You lose"
def play_game():
    print(logo)
    user_cards = []
    computer_cards = []
    is_game_over = False
    for x in range(2):
        user_cards.append(deal_card())
        computer_cards.append(deal_card())

    while not is_game_over:

        print(f"Your cards: {user_cards}, current score: {sum(user_cards)}")
        print(f"Computer's first cards: {computer_cards[0]}")


        if sum(user_cards)>=21 or sum(computer_cards)==21:
            is_game_over = True
        else:
            new_card = input("If you want to draw another card write 'y', if 'n' then the game has ended: ")
            if new_card == 'y' :
                user_cards.append(deal_card())  
            else:
                is_game_over = True     

    while sum(computer_cards) != 0 and sum(computer_cards)<17:
        computer_cards.append(deal_card())

    print(f"Your final hand: {user_cards}, final score: {sum(user_cards)}")
    print(f"Computer final hand: {computer_cards}, final score: {sum(computer_cards)}")
    print(compare(sum(user_cards),sum(computer_cards)))

while input("Do you want to play a game of Blackjack? Type 'y' or 'n': ") == 'y':
    play_game()